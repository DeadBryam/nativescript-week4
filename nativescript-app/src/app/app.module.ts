import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ListadoService } from "./domain/listado.service";
import {
    FavoritoState,
    reducersFavorito,
    InitFavoritoState
} from "./domain/fav.model";
import { ActionReducerMap, StoreModule as NgrxStoreModule  } from "@ngrx/store";


//redux
export interface AppState {
    favoritos: FavoritoState;
}

const reducers: ActionReducerMap<AppState> = {
    favoritos: reducersFavorito
};

const reducersInitialState = {
    favoritos: InitFavoritoState()
};

@NgModule({
    bootstrap: [AppComponent],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgrxStoreModule.forRoot(reducers,{initialState: reducersInitialState})
    ],
    declarations: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [ListadoService]
})
export class AppModule {}
