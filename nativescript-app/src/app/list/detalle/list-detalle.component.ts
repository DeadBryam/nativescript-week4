import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ListadoService } from "../../domain/listado.service";
import { RouterExtensions } from "nativescript-angular/router";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";
// import { registerElement } from "nativescript-angular/element-registry";

// registerElement(
//     "PullToRefresh",
//     () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh
// );

@Component({
    selector: "List-Delete",
    templateUrl: "./list-detalle.component.html",
    styleUrls: ["./../list.component.scss"]
})
export class ListDetalleComponent implements OnInit {
    constructor(
        private listado: ListadoService,
        private _routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {
        this.listado.addRandomPuntuacion();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onPull(args) {
        const pullRefresh = args.object;
        setTimeout(() => {
            this.listado.addRandomPuntuacion();
            pullRefresh.refreshing = false;
        }, 1000);
    }

    onItemTap(x) {
        dialogs
            .confirm({
                title: "Eliminar",
                message: "Desea eliminar el registro?",
                okButtonText: "Si",
                cancelButtonText: "No"
            })
            .then(result => {
                if (result == true) {
                    this.listado.deletePuntuacion(x.index);
                    Toast.makeText("Eliminado correctamente").show();
                }
            });
    }

}
