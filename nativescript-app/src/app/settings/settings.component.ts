import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as localStorage from "nativescript-localstorage";
import { RouterExtensions } from "nativescript-angular/router";
// let LS = require('nativescript-localstorage')

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    user: string;

    constructor(private _routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.user = localStorage.getItem("user");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    changePage(page: string) {
        this._routerExtensions.navigate([page], {
            transition: {
                name: "fade"
            }
        });
    }
}
